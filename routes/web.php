<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

/**
 * Authorized user area.
 */
Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home'); // Home controller.

    // New person routes.
    Route::get('/api/dane/new', 'Front\PersonController@createNewPersonForm')->name('create.person.form');
    Route::post('/api/dane/new', 'Front\PersonController@pushNewPerson')->name('create.person');

    // Database routes.
    Route::get('/api/dane/{type?}', 'Front\PersonController@listPersons')->name('list.person');

    // Edit person routes.
    Route::get('/api/dane/edit/{id}', 'Front\PersonController@editPersonForm')->name('edit.person');
    Route::post('/api/dane/edit/{id}', 'Front\PersonController@pushEditPerson');
    Route::delete('/api/dane/edit/{id}', 'Front\PersonController@destroyPerson')->name('destroy.person');
});

