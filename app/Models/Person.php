<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

final class Person extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'persons';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
       'name', 'surname', 'email', 'age', 'gender', 'intake', 'comments', 'payed',
   ];
}
