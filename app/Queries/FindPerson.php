<?php

namespace App\Queries;

use App\Models\Person;
use Illuminate\Contracts\Pagination\Paginator;

final class FindPerson
{
    /**
      * Find all persons by intake. Paginate results.
      *
      * @param string $intake
      * @return Paginator
    */
    public static function getByIntake($intake = '2019'): Paginator
    {
        switch ($intake) {
            case '2019':
                return Person::where('intake', '2019')->simplePaginate(15);
            
            case '2020':
                return Person::where('intake', '2020')->simplePaginate(15);
        }
    }

    /**
     * FInd all persons by payed.
     *
     * @return Paginator
     */
    public static function getByPayed(): Paginator
    {
        return Person::where('payed', 'yes')
            ->simplePaginate(15);
    }

    /**
      * Find or fail (depends on given boolean value - standard; false) person by primary key. Use given value.
      *
      * @param integer $personId
      * @param boolean $fail
      * @return Person|null
    */
    public static function findByPrimaryKey(int $personId, bool $fail = false): ?Person
    {
        return ($fail)
            ? Person::findOrFail($personId)
            : Person::find($personId);
    }
}