<?php

namespace App\Http\Controllers\Front;

use App\Queries\FindPerson;
use App\Jobs\Person\CreatePerson;
use App\Jobs\Person\UpdatePerson;
use App\Jobs\Person\DestroyPerson;
use App\Http\Controllers\Controller;
use App\Http\Requests\Front\EditPersonRequest;
use App\Http\Requests\Front\CreatePersonRequest;

final class PersonController extends Controller
{
    /**
     * Create new person form.
     *
     * @return void
     */
    public function createNewPersonForm()
    {
        return view('front.person.create');
    }

    /**
     * Push new perosn.
     *
     * @return void
     */
    public function pushNewPerson(CreatePersonRequest $request)
    {
        // Dispatch job to create new person.
        $this->dispatchNow(CreatePerson::createPersonRequest($request));

        // Flash message for user, return back to last page.
        return back()->with('status', __('Person successfully added.'));
    }

    /**
      * List persons.
      *
      * @param string|null $type
      * @return void
    */
    public function listPersons(string $type = null)
    {
        // Check if type is in route.
        if ($type && ! in_array($type, ['intake-2019', 'intake-2020', 'payed'])) {
            return abort(404);
        }

        // Switch on type from route.
        switch ($type) {
            case 'intake-2019':
                $persons = FindPerson::getByIntake();
                break;
            
            case 'intake-2020':
                $persons = FindPerson::getByIntake('2020');
                break;

            case 'payed':
                $persons = FindPerson::getByPayed();
                break;
            
            default:
                $persons = FindPerson::getByIntake();
        }

       // Compact data, send to view.
       return view('front.person.index', compact('persons'));
    }

    /**
     * Edit person form.
     *
     * @param integer $personId
     * @return void
     */
    public function editPersonForm(int $personId)
    {
        // Find person by given ID.
        $person = FindPerson::findByPrimaryKey($personId, $fail = true);

        // Compact data, send to view.
        return view('front.person.edit', compact('person'));
    }

    /**
     * Push edit person form.
     *
     * @param EditPersonRequest $request
     * @param integer $personId
     * @return void
     */
    public function pushEditPerson(EditPersonRequest $request, int $personId)
    {
        // Find person by given ID.
        $person = FindPerson::findByPrimaryKey($personId, $fail = false);
        
        // Dispatch job to update person.
        $this->dispatchNow(new UpdatePerson($person, $request->all()));

        // Flash message for user, return back to last page.
        return back()->with('status', __('Person successfully updated.'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(News $news)
    {
        // Dispatch job to destroy news
        $this->dispatchNow(new DeleteNews($news));

        // Flash success
        Alert::success(trans('admin/partials/alerts.informations.news_successfully_deleted'))->autoclose(2000);
        return redirect()->route('news.index');
    }

    /**
     * Destroy person.
     *
     * @param integer $personId
     * @return void
     */
    public function destroyPerson(int $personId)
    {
        // Find person by given ID.
        $person = FindPerson::findByPrimaryKey($personId, true);

        // Dispatch job to destroy person.
        $this->dispatchNow(new DestroyPerson($person));

        // Flash message for user, return back to last page.
        return redirect()->route('list.person')->with('status', __('Person successfully removed.'));
    }
}