<?php

namespace App\Http\Requests\Front;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

final class CreatePersonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:120'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:persons'],
            'age' => ['required', 'numeric'],
            'gender' => ['required', Rule::in(['male', 'female'])],
            'intake' => ['required', Rule::in(['2019', '2020'])],
            'comments' => ['required', 'string', 'max:255'],
            'payed' => ['required', Rule::in(['yes', 'no'])]
        ];
    }

    /**
     * Name.
     *
     * @return string
     */
    public function name(): string
    {
        return $this->get('name');
    }

    /**
     * Surname.
     *
     * @return string
     */
    public function surname(): string
    {
        return $this->get('surname');
    }

    /**
     * E-mail.
     *
     * @return string
     */
    public function email(): string
    {
        return $this->get('email');
    }

    /**
     * Age.
     *
     * @return string
     */
    public function age(): string
    {
        return $this->get('age');
    }

    /**
     * Gender.
     *
     * @return string
     */
    public function gender(): string
    {
        return $this->get('gender');
    }
    
    /**
     * Intake.
     *
     * @return string
     */
    public function intake(): string
    {
        return $this->get('intake');
    }

    /**
     * Comments.
     *
     * @return string
     */
    public function comments(): string
    {
        return $this->get('comments');
    }

    /**
     * Payed.
     *
     * @return string
     */
    public function payed(): string
    {
        return $this->get('payed');
    }

    /**
     * Return response with array of $errors.
     */
    public function response(array $errors)
    {
        return \Redirect::back()->withErrors($errors)->withInput();
    }
}
