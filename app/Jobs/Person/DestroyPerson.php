<?php

namespace App\Jobs\Person;

use App\Models\Person;

final class DestroyPerson
{
    /**
     * Person.
     *
     * @var Person
     */
    private $person;

    /**
     * Create a new job instance.
     *
     * @param Person $person
     */
    public function __construct(Person $person)
    {
        $this->person = $person;
    }

    /**
     * Execute the job.
     *
     * @return bool
     */
    public function handle(): bool
    {
        // Delete person.
        return $this->person->delete();
    }
}
