<?php

namespace App\Jobs\Person;

use App\Models\Person;
use Illuminate\Support\Arr;

final class UpdatePerson
{
    /**
     * Person.
     *
     * @var Person
     */
    private $person;

    /**
     * Person data.
     *
     * @var array
     */
    private $personData;

    /**
     * Create a new job instance. Filter given array keys.
     *
     * @param Person $person
     * @param array  $personData
     */
    public function __construct(Person $person, array $personData)
    {
        $this->person = $person;
        $this->personData = Arr::only($personData, ['name', 'surname', 'email', 'age', 'gender', 'intake', 'comments', 'payed']);
    }

    /**
     * Execute the job.
     *
     * @return bool
     */
    public function handle(): bool
    {
        // Update person.
        return $this->person->update($this->personData);
    }
}
