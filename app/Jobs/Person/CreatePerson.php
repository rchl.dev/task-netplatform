<?php

namespace App\Jobs\Person;

use App\Models\Person;
use App\Http\Requests\Front\CreatePersonRequest;

final class CreatePerson 
{
    /**
     * Name.
     *
     * @var string
     */
    private $name;

    /**
     * Surname.
     *
     * @var string
     */
    private $surname;

    /**
     * Email.
     *
     * @var string
     */
    private $email;

    /**
     * Age.
     *
     * @var integer
     */
    private $age;

    /**
     * Gender.
     *
     * @var string
     */
    private $gender;

    /**
     * Intake.
     *
     * @var string
     */
    private $intake;

    /**
     * Comments.
     *
     * @var string
     */
    private $comments;

    /**
     * Payed.
     *
     * @var string
     */
    private $payed;

    /**
      * Create a new job instance.
      * Accept only fields that are not forbbiden for this Person model and CreatePerson job.
      *
      * @param string $name
      * @param string $surname
      * @param string $email
      * @param integer $age
      * @param string $gender
      * @param string $intake
      * @param string $comments
      * @param string $payed
    */
    public function __construct(string $name, string $surname, string $email, int $age, string $gender, string $intake, string $comments, string $payed)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->email = $email;
        $this->age = $age;
        $this->gender = $gender;
        $this->intake = $intake;
        $this->comments = $comments;
        $this->payed = $payed;
    }

    /**
     * Create new person request.
     *
     * @param CreatePersonRequest $request
     * @return self
     */
    public static function createPersonRequest(CreatePersonRequest $request): self
    {
        return new static($request->name(), $request->surname(), $request->email(), $request->age(), $request->gender(), 
            $request->intake(), $request->comments(), $request->payed());
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Create person model.
        $person = new Person([
            'name' => $this->name,
            'surname' => $this->surname,
            'email' => $this->email,
            'age' => $this->age,
            'gender' => $this->gender,
            'intake' => $this->intake,
            'comments' => $this->comments,
            'payed' => $this->payed
        ]);

        // Save entry.
        return $person->save();
    }
}
