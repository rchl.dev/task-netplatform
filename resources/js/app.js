/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$(document).ready(function () {
    $('#deletePersonModalForm')
        .on('show.bs.modal', function (event) {
            const deleteButton = $(event.relatedTarget);
            $('#deletePersonalModalForm').attr('action', '/api/dane/edit/' + deleteButton.data('person-id'));
        });
});
