@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body">

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" id="editUser">
                        @csrf

                        <input type="hidden" name="personId" value="{{ $person->id }}">

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $person->name }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="surname" class="col-md-4 col-form-label text-md-right">{{ __('Surname') }}</label>

                            <div class="col-md-6">
                                <input id="surname" type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" value="{{ $person->surname }}" required autocomplete="surname">

                                @error('surname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $person->email }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="age" class="col-md-4 col-form-label text-md-right">{{ __('Age') }}</label>

                            <div class="col-md-6">
                                <input id="age" type="number" class="form-control @error('age') is-invalid @enderror" name="age" value="{{ $person->age }}" required>

                                @error('age')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>

                            <div class="col-md-6">
                                <select id="gender" name="gender" class="custom-select form-control @error('gender') is-invalid @enderror" required>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>

                                @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="intake" class="col-md-4 col-form-label text-md-right">{{ __('Intake') }}</label>

                            <div class="col-md-6">
                                <select id="intake" name="intake" class="custom-select form-control @error('intake') is-invalid @enderror" required>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                </select>

                                @error('intake')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="comments" class="col-md-4 col-form-label text-md-right">{{ __('Comments') }}</label>

                            <div class="col-md-6">
                                <textarea id="comments" name="comments" class="form-control @error('comments') is-invalid @enderror" required>{{ $person->comments }}</textarea>

                                @error('comments')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="payed" class="col-md-4 col-form-label text-md-right">{{ __('Payed') }}</label>

                            <div class="col-md-6">
                                <div class="form-check">
                                    <input id="payed-radio-checkbox" name="payed" class="form-check-input @error('payed') is-invalid @enderror" type="checkbox" value="yes" @if ($person->payed === 'yes') checked="checked" @endif>
                                    <label class="form-check-label" for="payed-radio-checkbox">
                                        {{ __('Yes') }}
                                    </label>
                                </div>

                                <div class="form-check">
                                    <input id="payed-radio-checkbox" name="payed" class="form-check-input @error('payed') is-invalid @enderror" type="checkbox" value="no" @if ($person->payed === 'no') checked="checked" @endif>
                                    <label class="form-check-label" for="payed-radio-checkbox">
                                        {{ __('No') }}
                                    </label>
                                </div>

                                @error('payed')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button person="{{ $person->id }}" class="btn btn-success">{{ __('Save') }}</button>

                                <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#deletePerson" data-person-id="{{ $person->id }}">{{ __('Delete') }}</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@include('modals.delete')

@endsection
