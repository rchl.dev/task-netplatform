@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body">

                    <ul class="list-group list-group-horizontal mb-3">
                        <li class="list-group-item">
                            <a href="{{ route('list.person', ['type' => 'intake-2019']) }}">{{ __('Intake 2019') }}</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('list.person', ['type' => 'intake-2020']) }}">{{ __('Intake 2020') }}</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('list.person', ['type' => 'payed']) }}">{{ __('Payed') }}</a>
                        </li>
                    </ul>

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (! $persons->isEmpty())
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">{{ __('Name') }}</th>
                                    <th scope="col">{{ __('Surname') }}</th>
                                    <th scope="col">{{ __('Age') }}</th>
                                    <th scope="col">{{ __('E-mail') }}</th>
                                    <th scope="col">{{ __('Gender') }}</th>
                                    <th scope="col">{{ __('Intake') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($persons as $person)
                                    <tr>
                                        <td><a href="{{ route('edit.person', ['id' => $person->id]) }}"> {{ $person->name }} </a></td>
                                        <td>{{ $person->surname }}</td>
                                        <td>{{ $person->age }}</td>
                                        <td>{{ $person->email }}</td>
                                        <td>{{ $person->gender }}</td>
                                        <td>{{ $person->intake }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        {{ $persons->links() }}
                    @else
                        <div class="alert alert-warning" role="alert">
                            {{ __('There is no data yet here.') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
